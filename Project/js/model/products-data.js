export let productsData = [
  {
    name: "iphoneX",
    price: "1000",
    screen: "screen 68",
    backCamera: "2 camera 12 MP",
    frontCamera: "7 MP",
    img: "https://cdn.tgdd.vn/Products/Images/42/114115/iphone-x-64gb-hh-600x600.jpg",
    // img2: "https://dienthoaihay.vn/images/products/2020/06/21/original/iphone-x-bac_1592725329.jpg.jpg",
    desc: "Thiết kế mang tính đột phá",
    type: "Iphone",
  },
  {
    name: "Samsung Galaxy M51 ",
    price: "3500",
    screen: "screen 69",
    backCamera: " Chính 64 MP & Phụ 12 MP, 5 MP, 5 MP",
    frontCamera: " 32 MP",
    img: "https://cdn.tgdd.vn/Products/Images/42/217536/samsung-galaxy-m51-trang-new-600x600-600x600.jpg",
    // img2: "https://www.xtmobile.vn/vnt_upload/product/03_2021/thumbs/600_galaxy_m51_trang_xtmobile.jpg",
    desc: '"Thiết kế đột phá, màn hình tuyệt đỉnh"',
    type: "Samsung",
  },
  {
    name: "Samsung Galaxy M22",
    price: "45000",
    screen: "screen 70",
    backCamera: "Chính 12 MP & Phụ 64 MP, 12 MP",
    frontCamera: " 32 MP",
    img: "https://cdn.tgdd.vn/Products/Images/42/240211/samsung-galaxy-m22-1-600x600.jpg",
    // img2: "https://www.xtmobile.vn/vnt_upload/product/05_2022/thumbs/600_samsung_m22_trang.jpg",
    desc: "Thiết kế mang tính đột phá",
    type: "Samsung",
  },
  {
    name: "Iphone 11",
    price: "1000",
    screen: "screen 54",
    backCamera: "Camera: Chính 12 MP & Phụ 64 MP, 12 MP",
    frontCamera: "32 MP",
    img: "https://didongviet.vn/pub/media/catalog/product//i/p/iphone-11-pro-max-256gb-didongviet_23.jpg",
    // img2: "https://cdn.tgdd.vn/Products/Images/42/188705/iphone-11-pro-vang-600x600-600x600.jpg",
    desc: "Thiết kế đột phá, màn hình tuyệt đỉnh",
    type: "Iphone",
  },
];

// const dataList = [];

// const getData = async () => {
//   const data = await fetch(
//     "https://633ec0420dbc3309f3bc527c.mockapi.io/Products"
//   ).then((res) => res.json());
//   dataList.push(data);
// };
// getData();
// console.log("dataList: ", dataList);

// export let productsData = [...dataList];

// console.log("productsData: ", productsData);
