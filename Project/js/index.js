import { renderProductsList } from "./controller/productController.js";
import { productsData } from "./model/products-data.js";

let myCart = [];

renderProductsList(productsData);

let searchProducts = (value, list) => {
  let filteredData = [];

  for (var i = 0; i < list.length; i++) {
    value = value.toLowerCase();
    let typeOfProduct = list[i].type.toLowerCase();
    if (typeOfProduct.includes(value)) {
      filteredData.push(list[i]);
    }
  }
  return filteredData;
};

document.getElementById("inputForm").onchange = function () {
  let valueSelect = document.getElementById("inputForm").value;

  let searchProduct = searchProducts(valueSelect, productsData);
  if (searchProduct) {
    renderProductsList(searchProduct);
  } else {
    renderProductsList(productsData);
  }
};

document.getElementById("miniCart").innerHTML = cartContent();

btnCloseCart();
function cartContent() {
  return `
  <div id="staplesbmincart">
  <div class="sbmincart-suc-cart-text">Cart</div>
  <button type="button" class="btn sbmincart-closer" id="closeCart">
    ×
  </button>

  <ul style="display: none"></ul>

  <div class="sbmincart-footer">
    ${emptyCart()}
  </div>
</div>`;
}

function emptyCart() {
  return `<p class="sbmincart-empty-text">Your shopping cart is empty</p>`;
}

let btnShow = document.getElementById("btnMyCart");
btnShow.addEventListener("click", function () {
  document.getElementById("staplesbmincart").style.display = "block";
});

function btnCloseCart() {
  let btnClose = document.getElementById("closeCart");
  btnClose.addEventListener("click", function () {
    document.getElementById("staplesbmincart").style.display = "none";
  });
}

let btnAddToCart = document.querySelectorAll(".single-item > button");
btnAddToCart.forEach(function (button, index) {
  button.addEventListener("click", function (event) {
    let gettTransmitvClass = event.target.parentElement;
    let geyItemImageClass = gettTransmitvClass.parentElement;
    let getItemContent = geyItemImageClass.parentElement;

    let imgProduct = getItemContent.querySelector("img").src;
    let nameProduct =
      getItemContent.querySelector(".item__price > h3").innerText;
    let priceProduct = getItemContent.querySelector(".price").innerText;

    let productQtity = 1;

    for (var index in myCart) {
      if (myCart[index].name == nameProduct) {
        productQtity = myCart[index].qty;
      }
    }

    let e = {
      name: nameProduct,
      price: priceProduct,
      img: imgProduct,
      qty: productQtity,
    };

    myCart.push(e);

    addCart(e);
    // cartTotal();
    deleteProductOfCart();
    checkOut();

    console.log("myCart: ", myCart);
  });
});

function cartItem(e = {}) {
  let {
    name: nameProduct,
    price: priceProduct,
    imgSrc: imgProduct,
    qty: amountProduct,
  } = e;
  return `

    <div class="sbmincart-details-name">
      <a
        class="sbmincart-name"
        href="${imgProduct}"
        ><span>${nameProduct}</span></a
      >
      <ul class="sbmincart-attributes"></ul>
    </div>
    <div class="sbmincart-details-quantity">
      <input
        class="sbmincart-quantity"
        data-sbmincart-idx="0"
        name="quantity_1"
        type="text"
        pattern="[0-9]*"
        value="${amountProduct}"
        autocomplete="off"
      />
    </div>
    <div class="sbmincart-details-remove">
      <button
        type="button"
        class="sbmincart-remove"
        data-sbmincart-idx="0"
      >
        ×
      </button>
    </div>
    <div class="sbmincart-details-price">
      $<span class="sbmincart-price">${priceProduct}</span>
    </div>
 `;
}

function addCart(e) {
  let liTagCreate = document.createElement("li");
  liTagCreate.classList.add("sbmincart-item");

  let ulContent = document.querySelector("#staplesbmincart > ul");
  let liContent = document.querySelectorAll("#staplesbmincart > ul li");
  let nameList = document.querySelectorAll(".sbmincart-name > span");

  for (var i = 0; i < liContent.length; i++) {
    if (nameList[i].innerHTML == e.name) {
      // remove old li tag before create new li tag
      liContent[i].parentNode.removeChild(liContent[i]);

      // notify if this product had been in cart
      alert("Product has already existed.");
      e.qty++;
      liTagCreate.innerHTML = cartItem(e);
    }
  }

  liTagCreate.innerHTML = cartItem(e);
  ulContent.style.display = "block";
  ulContent.append(liTagCreate);
  cartTotal();
}

function cartTotal() {
  let liContent = document.querySelectorAll("#staplesbmincart > ul li");

  let total = 0;

  for (var i = 0; i < liContent.length; i++) {
    let productQuantity =
      liContent[i].querySelector(".sbmincart-quantity").value * 1;
    let productPrice =
      liContent[i].querySelector(".sbmincart-price").innerText * 1;

    total += productPrice * productQuantity;
  }

  // show price total
  let cartFooter = document.querySelector(".sbmincart-footer");
  cartFooter.innerHTML = showTotalPrice(
    total.toLocaleString("en-US", { style: "currency", currency: "USD" })
  );
  quantityChange();
}

function showTotalPrice(total) {
  return `
    <div class="sbmincart-subtotal">Subtotal: <span>${total}</span> USD</div>
    <button
      id="checkout-cart"
      class="sbmincart-submit"
      type="submit"
      data-sbmincart-alt="undefined"
    >
      Check Out
    </button>
  `;
}

function deleteProductOfCart() {
  // let ulContent = document.querySelector("#staplesbmincart > ul");
  let liContent = document.querySelectorAll("#staplesbmincart > ul li");

  for (var i = 0; i < liContent.length; i++) {
    let removeBtnList = document.querySelectorAll(".sbmincart-details-remove");
    removeBtnList[i].addEventListener("click", function (event) {
      let deleteBtn = event.target.parentElement;
      let selectedCartItem = deleteBtn.parentElement;
      selectedCartItem.remove();
      // console.log("selectedCartItem: ", selectedCartItem);
      cartTotal();
    });
  }
  btnCloseCart();
}

function quantityChange() {
  let liContent = document.querySelectorAll("#staplesbmincart > ul li");

  for (var i = 0; i < liContent.length; i++) {
    let quantityValue = liContent[i].querySelector(".sbmincart-quantity");
    quantityValue.addEventListener("change", () => {
      cartTotal();
    });
  }
  checkOut();
}

function checkOut() {
  document.querySelector(".sbmincart-submit").addEventListener("click", () => {
    let liContent = document.querySelectorAll("#staplesbmincart > ul li");
    // get value of subtotal in My Cart
    let totalPrice = document.querySelector(
      ".sbmincart-subtotal > span"
    ).innerText;

    alert(`Your whole payment obligation is ${totalPrice} USD`);
    for (var i = 0; i < liContent.length; i++) {
      liContent[i].remove();
    }
    document.getElementById("miniCart").innerHTML = cartContent();
    myCart = [];
    btnCloseCart();
  });
}
