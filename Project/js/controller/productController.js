import { Product } from "../model/productModel.js";

export let renderProductsList = (list) => {
  let contentHTML = "";

  list.forEach((element) => {
    let object = new Product(
      element.id,
      element.name,
      element.price,
      element.screen,
      element.backCamera,
      element.frontCamera,
      element.img,
      element.desc,
      element.type
    );
    let eachProduct = JSON.stringify(object);
    // console.log("eachProduct: ", eachProduct);
    contentHTML += `
    <div class="product__item col-lg-3 col-6 mt-5">
    <div class="item__content">
      <div class="item__image">
        <a href="#">
          <img
            class="pic-1"
            src="${object.img}"
          />
          <!-- <img class="pic-2" src="${object.img2}" /> -->
        </a>

        <ul class="social">
          <li>
            <a href="#" data-tip="Quick View"
              ><span class="fa fa-eye"></span
            ></a>
          </li>

          <li>
            <a href="#" data-tip="Add to Cart"
              ><span class="fa fa-shopping-bag"></span
            ></a>
          </li>
        </ul>

        <div class="transmitv single-item">
          <button
            type="submit"
            class="transmitv-cart ptransmitv-cart add-to-cart"
          >
            Add to Cart
          </button>
        </div>
      </div>
      <div class="item__price">
        <h3 class="title"><a href="#">${object.name}</a></h3>
        <div class="text_price">
          <del>$575.00</del> $<span class="price"> ${object.price} </span>
        </div>
        <input type="hidden" name="upload" value="1" />
      </div>
    </div>
  </div>
    `;
  });
  document.getElementById("listProducts").innerHTML = contentHTML;
};

export function syncState(state) {
  localStorage.setItem("state", JSON.stringify(state));
}
