// example link to run
import { getInfoFromForm } from "./../controller/productController.js";

// official link to deploy
const BASE_URL = "https://633ec0420dbc3309f3bc527c.mockapi.io";

export let getListProductServ = () => {
  return axios({
    url: `${BASE_URL}/Products`,
    method: "GET",
  });
};

export let postCreateProductServ = (products) => {
  return axios({
    url: `${BASE_URL}/Products`,
    method: "POST",
    data: products,
  });
};

export let deleteProductIdServ = (id) => {
  return axios({
    url: `${BASE_URL}/Products/${id}`,
    method: "DELETE",
  });
};

export let editProductServ = (id) => {
  return axios({
    url: `${BASE_URL}/Products/${id}`,
    method: "PUT",
  });
};
